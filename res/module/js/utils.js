layui.define(["laytpl", "laypage", "layer", "form"], function(exports) {
	var $ = layui.$;
	var obj = {
		apiUrl: function(str) {
			var urls = {
				LOGIN: "/api/platform/sysuser/login",
				LOGOUT: "/api/platform/sysuser/logout",
				RESET_PASSWORD: "/api/platform/sysuser/resetPassword",
				VERIFY_USERNAME: "/api/platform/sysuser/verifyUserName",
				MACHINE_LIST: "/api/platform/drillingCrew/drillingCrewList",
				UNBIND_DC: "/api/platform/drillingCrew/drillingCrewList/unbindDc",
				REGISTER_DC: "/api/platform/drillingCrew/register",
				MERCHANTS_LIST: "/api/platform/drillingCrew/merchantsList",
				VERIFY_DC_NUM: "/api/platform/drillingCrew/drillingCrewList/verifyDcNum",
				CANCEL_DC: "/api/platform/drillingCrew/drillingCrewList/cancelDc",
				BIND_DC: "/api/platform/drillingCrew/drillingCrewList/bindDc",
				ACCOUNT_LIST: "/api/platform/account/list",
				DELETE_ACCOUNT: "/api/platform/account/delete",
				REGISTER_ACCOUNT: '/api/platform/account/register',
				RESET_PASSWORD: '/api/platform/account/resetPassword',
				VERIFY_ACCOUNT_NAME: '/api/platform/account/verifyAccountName',
				VIEW_ACCOUNT: '/api/platform/account/viewAccount',
				UPDATE_ACCOUNT: '/api/platform/account/update',
				MERCHANT_LIST: '/api/platform/merchants/merchantsList',
				VERIFY_DC_NUM: '/api/platform/merchants/merchantsList/verifyDcNum',
				VERIFY_NAME: '/api/platform/merchants/merchantsList/verifyName',
				REGISTER_MERCHANT: '/api/platform/merchants/register'
			};
			return urls[str];
		},
		msg: function(str) {
			layer.msg(str, {
				skin: 'dialog-class'
			});
		},
		isNullObj: function(obj) {
			for(var key in obj) {
				return false;
			}
			return true;
		},
		isNullStr: function(str) {
			if(str == null || str == '' || str == undefined) {
				return true;
			}
			return false;
		},
		promptErrInfo: function(str, parentNode) {
			parentNode.find(".prompt-info-panel").show();
			parentNode.find(".prompt-info-span").html(str);
		},
		promptCorInfo: function(parentNode) {
			parentNode.find(".prompt-info-panel").hide();
		},
		checkMobile: function(str) {
			if(!(/^1[0-9]{10}$/.test(str))) {
				return false;
			}
			return true;
		},
		isNullForm: function(id) {
			if(obj.isNullStr($("#" + id).val())) {
				return true;
			}
			return false;
		},
		errorStyle: function(_this) {
			_this.next("img").attr('src', 'img/error1.png');
			_this.next("img").css('display', 'inline-block');
		},
		correctStyle: function(_this) {
			_this.next("img").attr('src', 'img/correct1.png');
			_this.next("img").css('display', 'inline-block');
		},
		isExistError: function() {
			let verifyImg = $(".verify-img");
			let isExistError = false;
			verifyImg.each(function() {
				let _this = $(this);
				let imgSrc = _this.attr("src");
				if(imgSrc.indexOf('error1.png') != -1) {
					isExistError = true;
					return false;
				}
			});
			return isExistError;
		}
	}

	//输出test接口
	exports('utils', obj);
});