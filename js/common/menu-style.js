let htmlName = getHtmlName();
console.log("htmlName="+htmlName);
// $('.'+htmlName+'-li').addClass('current');
console.log(document.getElementsByClassName('machine-li'));
document.getElementsByClassName(htmlName+'-li')[0].classList.add('current');

console.log(window.screen.width);

/***
 * 获取当前html名称
 */
function getHtmlName() {
    var url = window.location.href;
    var arrUrl = url.split("?");
    console.log('arrUrl='+arrUrl);
    var html = "";
    if(arrUrl == null || arrUrl.length <= 0) {
        //html = url.split('/')[4];
        html = url.split('/')[3];
        console.log('html'+html);
    } else {
        let urlIndex = arrUrl[0];
        html = urlIndex.split('/')[3];
    }
    return html.split('.')[0];
    //return html;
}

layui.use(['jquery', 'element', 'form', 'utils'], function() {
    var $ = layui.jquery;
    var active = {
        navTitle: function(othis) {
            let type = othis.data("type");
            let _this;
            /*let _other;*/
            if(type == "1") {
                _this = $(".add-account-panel");
                /*_other = $("#js-bar-content");*/
            } else if(type == "2") {
                _this = $("#js-bar-content");
                /*_other = $(".add-account-panel");*/
            } else if (type == "3") {
            	_this = $("#js-dc-content");
            }
            _this.is(':hidden') ? _this.show() : _this.hide();
            /*_other.hide();*/
            if($("#js-password-panel")){
            	if($("#js-password-panel").is(':hidden')) {
            		$("#js-password-panel").show()
            	}
            }
            //机台的修改按钮
            if($("#js-modify-btn-panel")){
            	if(!$("#js-modify-btn-panel").is(':hidden')) {
            		$("#js-modify-btn-panel").hide()
            	}
            }
            //机台的注册按钮
            if($("#js-register-btn-panel")){
            	if($("#js-register-btn-panel").is(':hidden')) {
            		$("#js-register-btn-panel").show()
            	}
            }
        },
        moreBtnOpt: function(othis) {
            /*let _this = $(".more-opt-span");*/
            let accountId = othis.data("id");
            let _this = $(".more-span-"+accountId);
            let moreBtn = $(".js-more-btn-"+accountId);
            _this.is(':hidden') ? _this.show() : _this.hide();
            _this.is(':hidden') ? moreBtn.find(".icon-14").html("&#xe623;"):moreBtn.find(".icon-14").html("&#xe625;")
        }
    }
    $('#js-body').on('click', '.optBtn', function() {
        var othis = $(this), method = othis.data('method');
        active[method] ? active[method].call(this, othis) : '';
    });
});