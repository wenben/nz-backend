layui.use(['jquery', 'element', 'form', 'utils'], function() {
	var $ = layui.jquery,
		utils = layui.utils;
	let roleTable = layui.sessionData('role');
	console.log(roleTable);
	if(utils.isNullObj(roleTable)) {
		layui.sessionData('error', {
			key: 'session',
			value: -1
		});
		window.location.href = "login.html";
	}
	$(".css-role-name").html(roleTable.roleName);
});